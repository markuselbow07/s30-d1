const express = require("express");
// require mongoose package
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// Connection to MongoDB Atlas
// Syntax: mongoose.connect("<MongoDB Atlas connection string>", { useNewUrlParser:true });
mongoose.connect("mongodb+srv://markusElbow:Magyarorszag7@cluster0.oy0gf.mongodb.net/b138_to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

let db = mongoose.connection;

// If a connection error occured, output in the console
// console.error.bind(console) allow us to print errors in the browser console and in the terminal
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud database"));


// Schema

const taskSchema = new mongoose.Schema({
	name : String,
	status : {
		type : String,
		default : "pending"
	}
})

// Create task model

const Task = mongoose.model("Task", taskSchema);


app.use(express.json());

app.use(express.urlencoded({extended:true}));
// Create  POST route to create a new task 
app.post("/tasks", (req, res) => {
	Task.findOne({name: req.body.name}, (err, result) => {
	if (result != null && result.name == req.body.name){
		return res.send("Duplicate task found");
	}
	else {
		let newTask = new Task ({
			name : req.body.name
		});
		newTask.save((saveErr, savedTask) => {
			if(saveErr){
				return console.error(saveErr);
			}
			else{
				return res.status(201).send("New task created");
			}
		})
	}
	})
})

// Create a GET request to retrieve all the tasks
app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) =>{
		// If an error occured
		if(err){
			return console.log(err);
		}
		// If no errors are found
		else{
			return res.status(200).json({
				data : result
			})
		}
	})
})

/*
1. Create a User schema.

2. Create a User model.

3. Create a POST route that will access the "/signup" route that will create a user.

4. Process a POST request at the "/users" route using postman to register a user.

5. Push to gitlab with the commit message of Add activity code.
*/


// Schemas

const userSchema = new mongoose.Schema({
	name : String,
	status : {
		type : String,
		default : "pending"
	}
})

// Create task model

const User = mongoose.model("User", userSchema);


app.use(express.json());

app.use(express.urlencoded({extended:true}));

// Create  POST route to create a new task 
app.post("/signup", (req, res) => {
	User.findOne({name: req.body.name}, (err, result) => {
	if (result != null && result.name == req.body.name){
		return res.send("Duplicate user found");
	}
	else {
		let newUser = new User ({
			name : req.body.name
		});
		newUser.save((saveErr, savedUser) => {
			if(saveErr){
				return console.error(saveErr);
			}
			else{
				return res.status(201).send("New user registered");
			}
		})
	}
	})
})

app.post("/signup", (req, res) => {
	User.find({}, (err, result) =>{
		// If an error occured
		if(err){
			return console.log(err);
		}
		// If no errors are found
		else{
			return res.status(200).json({
				data : result
			})
		}
	})
})
//
app.listen(port, () => console.log(`Server is running at port ${port}`));
